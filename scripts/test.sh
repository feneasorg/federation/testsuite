#!/bin/bash

# print bats output while writing results to file
truncate --size 0 report.tap && tail -f report.tap &

# run tests and save it as tap format
bats --tap $(find tests -name $PROJECT'*.bats') >> report.tap
# remember exit code for later
exitCode=$?

# generate junit
perl scripts/tap-to-junit-xml bats --input report.tap --output report.xml

exit $exitCode
