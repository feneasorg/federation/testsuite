#!/bin/bash

project=$(echo $CI_COMMIT_TAG |cut -d- -f2)

# change into docker project
cd $CI_PROJECT_DIR/projects/$project || exit 1

# authenticate with docker registry first
docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY || exit 1

# build the image locally
docker build -t \
  ${CI_REGISTRY_IMAGE}/${project}:$CI_COMMIT_TAG . || exit 1
# overwrite latest-tag with new image
docker tag ${CI_REGISTRY_IMAGE}/${project}:$CI_COMMIT_TAG \
  ${CI_REGISTRY_IMAGE}/${project}:latest || exit 1
# upload the image and both tags
docker push ${CI_REGISTRY_IMAGE}/${project}:$CI_COMMIT_TAG || exit 1
docker push ${CI_REGISTRY_IMAGE}/${project}:latest || exit 1
