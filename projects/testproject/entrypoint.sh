#!/bin/bash

if [ "$PROJECT" == "testproject" ]; then
  git checkout . \
    && git remote add custom $PRREPO \
    && git fetch custom \
    && git merge -m merge $PRSHA \
    && git log -1 || {
      echo "Cannot pull from repo=$PRREPO sha=$PRSHA"
      exit 1
    }
fi

go build -o server main.go || {
  echo "Cannot build sources"
  exit 1
}

./server :$PORT
