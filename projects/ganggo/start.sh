#!/bin/bash

root=$GOPATH/src/git.feneas.org/ganggo/ganggo
# parse and replace configuration values
sed -i "s/NAME/$DATABASE/g" $root/conf/app.conf \
  && sed -i "s/PORT/$PORT/g" $root/conf/app.conf \
  && sed -i "s/CIP/$(hostname -i)/g" $root/conf/app.conf || exit 1;

if [ "$(basename $PRREPO)" == "ganggo.git" ]; then
  # Use custom application server
  git checkout . \
    && git remote add custom $PRREPO \
    && git fetch custom \
    && git merge -m merge $PRSHA \
    && git log -1 || {
      echo "Cannot pull from repo=$PRREPO sha=$PRSHA"
      exit 1
    }
else
  # Update ganggo application server
  git pull origin master && git log -1 || {
    echo "Cannot pull from origin"
    exit 1
  }
fi
# install new dependencies
# and compile assets
make install precompile
dep ensure

# update federation lib if necessary
if [ "$(basename $PRREPO)" == "federation.git" ]; then
  repo=$root/vendor/git.feneas.org/ganggo/federation
  # go-dep prunes git-directory from vendor folder
  rm -rf $repo && git clone $PRREPO $repo
  cd $repo && git checkout $PRSHA
fi

# start the application server
revel run git.feneas.org/ganggo/ganggo
