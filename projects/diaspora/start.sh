#!/bin/bash

cd /diaspora \
  && sed -i "s/database: diaspora_production/database: $DATABASE/" config/database.yml \
  && sed -i "s/url: \".*\"/url: \"http:\/\/$(hostname -i):$PORT\/\"/" config/diaspora.yml \
  && sed -i "s/#redis:.*/redis: \"redis:\/\/redishost\/${PORT: -1}\"/" config/diaspora.yml \
  && bundle exec rails db:drop db:create db:migrate \
  && ./script/server
