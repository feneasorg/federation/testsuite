# vim:ft=sh
#
# All API calls are documented here: https://ganggo.github.io/api/
#

load '../test_helper'
load '../projects/ganggo/ganggo_helper'
load '../projects/socialhome/socialhome_helper'

@test "$PREFIX create databases" {
  create_database "g1"
  [ "$?" -eq 0 ]
  create_database "s1"
  [ "$?" -eq 0 ]
}

@test "$PREFIX start ganggo#1 server" {
  ganggo_start_server g1 "9000"
}

@test "$PREFIX start socialhome#1 server" {
  socialhome_start_server s1 "9001"
}

@test "$PREFIX create ganggo#1 user" {
  ganggo_create_user g1 "http://$(container_ip "g1"):9000"
}

@test "$PREFIX create socialhome#1 user" {
  socialhome_create_user s1
}

# since socialhome only delivers to subscriped servers
# the ganggo user has to follow the socialhome user
@test "$PREFIX fetch ganggo#1 token" {
  ganggo_fetch_token g1 "http://$(container_ip "g1"):9000"
}

@test "$PREFIX setup user relations" {
  ganggo_start_sharing \
    "s1@$(container_ip "s1"):9001" "http://$(container_ip "g1"):9000"
}

# otherwise the socialhome token request will fail
@test "$PREFIX clean-up ganggo#1 files" {
  rm -v $CURL_PARAMS
  [ "$?" -eq 0 ]
}

# according to http://socialhome.readthedocs.io/en/latest/api.html#authenticating
@test "$PREFIX fetch socialhome#1 token" {
  socialhome_fetch_token s1 "http://$(container_ip "s1"):9001"
}

@test "$PREFIX start sharing" {
  socialhome_start_sharing \
    "g1@$(container_ip "g1"):9000" "http://$(container_ip "s1"):9001"
}

@test "$PREFIX create public entities and check federation" {
  post "text=foobar&visibility=public" \
    "http://$(container_ip "s1"):9001/api/content/"
  [ "$?" -eq 0 ]
  uuid=$(json_value "uuid")
  echo "uuid = $uuid"
  [ "$uuid" != "null" ]
  # check post in ganggo
  function cmd() {
    query "g1" "select count(*) from posts
                where guid = '$uuid' and public = true;"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
}

@test "$PREFIX clean-up files, containers and database" {
  for app in g1 s1; do
    stop_app $app
    [ "$?" -eq 0 ]
    remove_app $app
    [ "$?" -eq 0 ]
    drop_database $app
    [ "$?" -eq 0 ]
  done
  rm -v $CURL_PARAMS
  [ "$?" -eq 0 ]
}
