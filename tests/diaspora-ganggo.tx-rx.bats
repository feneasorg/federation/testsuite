# vim:ft=sh

load '../test_helper'
load '../projects/ganggo/ganggo_helper'

@test "$PREFIX create database" {
  create_database "g1"
  [ "$?" -eq 0 ]
}

@test "$PREFIX start ganggo#1 server" {
  ganggo_start_server g1 "9000"
}

@test "$PREFIX start diaspora#1 server" {
  start_app "d1" "3000" "diaspora"$(latest_tag "diaspora")
  [ "$?" -eq 0 ]
  code=$(wait_for "docker logs $(container_id "d1")" "Starting Diaspora in production")
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
  # unicorn timeout
  sleep 15
}

@test "$PREFIX create ganggo user" {
  ganggo_create_user g1 "http://$(container_ip "g1"):9000"
}

@test "$PREFIX create diaspora user" {
  rails_runner d1 "User.build(
    username: 'd1', email: 'd1@d1.de', password: 'pppppp').save!"
  [ "$?" -eq 0 ]
}

@test "$PREFIX setup user relations" {
  rails_runner "d1" "user = User.find_by(username: 'd1')
    person = Person.find_or_fetch_by_identifier('g1@$(container_ip "g1"):9000')
    aspect = Aspect.new(name: 'test', user_id: user.id)
    aspect.save!
    user.share_with(person, aspect)"
  [ "$?" -eq 0 ]
}

function send_type() {
  type=$1

  # should we send it publicly or limited
  if [ "$type" == "private" ]; then
    postType="public: false, aspect_ids: user.aspect_ids"
  else
    postType="public: true"
  fi
  # send post via diaspora
  postID=$(rails_runner "d1" "user = User.find_by(username: 'd1');
            puts StatusMessageCreationService.new(user).create(
              status_message: {text: 'hello world'}, $postType
            ).id;")
  echo "postID = $postID"
  [ "$?" -eq 0 ]
  [ "$postID" -gt 0 ]

  # check if ganggo received it
  function cmd() {
    query "g1" "select count(*) from posts where public = true;"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]

  # send comment
  guid=$(rails_runner "d1" "user = User.find_by(username: 'd1');
          puts CommentService.new(user)
            .create($postID, 'commenttext').guid;")
  echo "guid = $guid"
  [ "$?" -eq 0 ]
  # XXX how to check guid
  #[ "$guid" != "null" ]

  # check comment
  function cmd() {
    query "g1" "select count(*) from comments where guid = '$guid';"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]

  # send like
  guid=$(rails_runner "d1" "user = User.find_by(username: 'd1');
          puts LikeService.new(user).create($postID).guid;")
  echo "guid = $guid"
  [ "$?" -eq 0 ]
  # XXX how to check guid
  #[ "$guid" != "null" ]

  # check comment
  function cmd() {
    query "g1" "select count(*) from likes where guid = '$guid';"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
}

@test "$PREFIX create public entities and check federation" {
  send_type public
}

@test "$PREFIX create private entities and check federation" {
  send_type private
}

@test "$PREFIX stop and delete containers" {
  for app in g1 d1; do
    stop_app $app
    [ "$?" -eq 0 ]
    remove_app $app
    [ "$?" -eq 0 ]
  done
}

@test "$PREFIX drop databases" {
  drop_database "g1"
  [ "$?" -eq 0 ]
  drop_database "d1"
  [ "$?" -eq 0 ]
}
