# vim:ft=sh
#
# All API calls are documented here: https://ganggo.github.io/api/
#

load '../test_helper'
load '../projects/ganggo/ganggo_helper'

@test "$PREFIX create database" {
  create_database "g1"
  [ "$?" -eq 0 ]
}

@test "$PREFIX start ganggo#1 server" {
  ganggo_start_server g1 "9000"
}

@test "$PREFIX start diaspora#1 server" {
  start_app "d1" "3000" "diaspora"$(latest_tag "diaspora")
  [ "$?" -eq 0 ]
  code=$(wait_for "docker logs $(container_id "d1")" "Starting Diaspora in production")
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
  # unicorn timeout
  sleep 15
}

@test "$PREFIX create user" {
  ganggo_create_user g1 "http://$(container_ip "g1"):9000"
}

@test "$PREFIX create diaspora user" {
  rails_runner d1 "User.build(
    username: 'd1', email: 'd1@d1.de', password: 'pppppp').save!"
  [ "$?" -eq 0 ]
}

@test "$PREFIX fetch user token" {
  ganggo_fetch_token g1 "http://$(container_ip "g1"):9000"
}

@test "$PREFIX setup user relations" {
  ganggo_start_sharing \
    "d1@$(container_ip "d1"):3000" "http://$(container_ip "g1"):9000"
}

function send_type() {
  type=$1
  endpoint="http://$(container_ip "g1"):9000"

  aspectID=0
  [ "$type" == "private" ] && aspectID=1

  # create post via ganggo
  post "post=helloworld&aspectID=$aspectID" "$endpoint/api/v0/posts"
  echo "expected 200, got $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  postID=$(json_value "ID")
  echo "body = $HTTP_BODY"
  echo "postID = $postID"
  [ "$postID" -gt 0 ]
  guid=$(json_value "Guid")
  echo "guid = $guid"
  [ "$guid" != "null" ]
  # check post in diaspora
  function cmd() {
    public="true"
    [ "$type" == "private" ] && public="false"
    query "d1" "select count(*) from posts
                where guid = '$guid' and public = $public;"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]

  # create comment
  post "comment=hellod1" "$endpoint/api/v0/posts/$postID/comments"
  echo "expected 200, got $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  guid=$(json_value "Guid")
  echo "body = $HTTP_BODY"
  echo "guid = $guid"
  [ "$guid" != "null" ]
  # check comment
  function cmd() {
    query "d1" "select count(*) from comments
                where guid = '$guid';"
  }
  code=$(wait_for cmd "1" 120)
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]

  # create like
  # XXX decryption issues .. investigate!!
  #post "" "$endpoint/api/v0/posts/$postID/likes/true"
  #echo "expected 200, got $HTTP_STATUS_CODE"
  #[ "$HTTP_STATUS_CODE" == "200" ]
  #guid=$(json_value "Guid")
  #echo "body = $HTTP_BODY"
  #echo "guid = $guid"
  #[ "$guid" != "null" ]
  ## check like
  #function cmd() {
  #  query "d1" "select count(*) from likes
  #              where guid = '$guid';"
  #}
  #code=$(wait_for cmd "1" 120)
  #echo "expected 0, got $code"
  #[ "$code" -eq "0" ]
}

@test "$PREFIX create public entities and check federation" {
  send_type public
}

@test "$PREFIX create private entities and check federation" {
  send_type private
}

@test "$PREFIX stop and delete containers" {
  for app in g1 d1; do
    stop_app $app
    [ "$?" -eq 0 ]
    remove_app $app
    [ "$?" -eq 0 ]
  done
}

@test "$PREFIX drop databases" {
  drop_database "g1"
  [ "$?" -eq 0 ]
  drop_database "d1"
  [ "$?" -eq 0 ]
}
