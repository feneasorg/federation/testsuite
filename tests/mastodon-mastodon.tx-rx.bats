# vim:ft=sh

load '../test_helper'

@test "$PREFIX start mastodon#1 server" {
  start_app "m1" "3000" "mastodon"$(latest_tag "mastodon")
  [ "$?" -eq 0 ]
  code=$(wait_for_mastodon "m1")
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
}

@test "$PREFIX start mastodon#2 server" {
  start_app "m2" "3001" "mastodon"$(latest_tag "mastodon")
  [ "$?" -eq 0 ]
  code=$(wait_for_mastodon "m2")
  echo "expected 0, got $code"
  [ "$code" -eq "0" ]
}

# see https://github.com/tootsuite/documentation/blob/master/Using-the-API/Testing-with-cURL.md
@test "$PREFIX create oauth application" {
  post 
    "client_name=bats&redirect_uris=urn:ietf:wg:oauth:2.0:oob&scopes=read%20write%20follow" \
    "http://$(container_ip "m1"):3000/api/v1/apps"
  [ "$HTTP_STATUS_CODE" == "200" ]
  client_id=$(json_value "client_id")
  [ "$client_id" != "" ]
  client_secret=$(json_value "client_secret")
  [ "$client_secret" != "" ]

  client_auth="client_id=$client_id&client_secret=$client_secret&grant_type=password"
  user_auth="username=admin@$(container_ip "m1"):3000&password=mastodonadmin"
  post "${client_auth}&${user_auth}&scope=read%20write%20follow" \
    "http://$(container_ip "m1"):3000/oauth/token"
  [ "$HTTP_STATUS_CODE" == "200" ]
  token=$(json_value "access_token")
  echo "token = $token"
  [ "$token" != "" ]
  echo "-H 'Authorization: Bearer $token'" > $CURL_PARAMS
}

@test "$PREFIX follow admin@m2:3001" {
  post "uri=admin@$(container_ip "m2"):3001" \
    "http://$(container_ip "m1"):3000/api/v1/follows"
  post "uri=admin@$(container_ip "m2"):3001" \
    "http://$(container_ip "m1"):3000/api/v1/follows"
  echo "HTTP_STATUS_CODE = $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  username=$(json_value "username")
  [ "$username" != "" ]
  [ "$username" == "admin" ]
}

@test "$PREFIX search for admin@m2:3001" {
  get "http://$(container_ip "m1"):3000/api/v1/search?q=admin@$(container_ip "m2"):3001"
  echo "HTTP_STATUS_CODE = $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  echo "HTTP_BODY = $HTTP_BODY"
  acct=$(echo $HTTP_BODY |jq -r '.accounts[] | select(.username == "admin") | .acct')
  [ "$acct" != "" ]
  [ "$acct" == "admin@$(container_ip "m2"):3001" ]
}

@test "$PREFIX clean-up containers, databases and temp files" {
  stop_app "m1 m2"
  [ "$?" -eq 0 ]
  remove_app "m1 m2"
  [ "$?" -eq 0 ]
  drop_database "m1"
  [ "$?" -eq 0 ]
  rm -v $CURL_PARAMS
  [ "$?" -eq 0 ]
}
