# vim:ft=sh
#
# this test file is related to https://feneas.git.feneas.org/federation/testsuite

load '../test_helper'

@test "$PREFIX start testproject#1 server" {
  start_app tp1 3000 testproject$(latest_tag testproject)
  [ "$?" -eq 0 ]
  # NOTE You can also wait for certain log output!
  # Incase your server is only operational after
  # a special message then you can use the wait_for
  # function from the test_helper file:
  #
  #   wait_for <command> <search-text>
  #
  # for this project that could look something like this:
  #
  cmd="docker logs $(container_id tp1)"
  text="Listening on :3000"
  code=$(wait_for "$cmd" "$text")
  [ "$code" -eq "0" ]
}

@test "$PREFIX start testproject#2 server" {
  start_app tp2 3001 testproject$(latest_tag testproject)
  [ "$?" -eq 0 ]
  cmd="docker logs $(container_id tp2)"
  text="Listening on :3001"
  code=$(wait_for "$cmd" "$text")
  [ "$code" -eq "0" ]
}

@test "$PREFIX ping both servers, should reply with pong" {
  get "http://$(container_ip tp1):3000/ping"
  echo "expected 200, got $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  echo "expected pong, got $HTTP_BODY"
  [ "$HTTP_BODY" == "pong" ]

  get "http://$(container_ip tp2):3001/ping"
  echo "expected 200, got $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  echo "expected pong, got $HTTP_BODY"
  [ "$HTTP_BODY" == "pong" ]
}

@test "$PREFIX ping testproject#1 via testproject#2 server" {
  get "http://$(container_ip tp2):3001/send?http://$(container_ip tp1):3000/ping"
  echo "expected 200, got $HTTP_STATUS_CODE"
  [ "$HTTP_STATUS_CODE" == "200" ]
  echo "expected ok, got $HTTP_BODY"
  [ "$HTTP_BODY" == "ok" ]

  # ping count should be two by now
  count=$(docker logs $(container_id tp1) 2>&1 | grep ping | wc -l)
  echo "expected 2, got $count"
  [ "$count" -eq "2" ]
}

@test "$PREFIX stop and delete the containers" {
  for tp in tp1 tp2; do
    stop_app $tp
    [ "$?" -eq 0 ]
    remove_app $tp
    [ "$?" -eq 0 ]
  done
}
