FROM debian

# install curl, git and the psql client
RUN apt-get update && apt-get install -y \
  curl git-core postgresql-client libxml-generator-perl

# install docker binary
RUN curl -L https://get.docker.com | bash -

# install jq command line json parser
RUN curl -o /usr/local/bin/jq -L \
  https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
RUN chmod +x /usr/local/bin/jq

# install bats unit testing in bash
RUN git clone --depth 1 https://github.com/bats-core/bats-core.git \
  && cd bats-core \
  && ./install.sh /usr/local
