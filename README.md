# Federation Tests

This test suite spins up pairs of instances for them to test simple federation
scenarios, testing for various protocols. At the moment it doesn't test conformance
to any protocol, but just ensures messages sent get received by the other end
of the instance pair at test.

## Documentation

You will find the full documentation at: [feneas.git.feneas.org/federation/testsuite](https://feneas.git.feneas.org/federation/testsuite/)

### Adding your software

You can add your software to the list of supported projects by copying the
`projects/testproject` directory and making your own helper and Dockerfile.
The helper is used in the tests, which are defined in the `tests` directory.
The Dockerfile is an adaptation of your software's Dockerfile, with the 
addition of a custom entrypoint, required by the test suite.

