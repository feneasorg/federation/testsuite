export PREFIX=$(basename $BATS_TEST_FILENAME)
export CURL_PARAMS="$BATS_TMPDIR/curl-params"
export TEST_ID="$BATS_TMPDIR/test-id$CI_JOB_ID"
export CI_REGISTRY_IMAGE="registry.git.feneas.org/feneas/federation/testsuite"

unset HTTP_STATUS_CODE
unset HTTP_BODY

# will run on every test and
# verify installed dependencies
function setup() {
  # postgresql client
  command -v psql
  [ "$?" -eq 0 ]
  # curl http client
  command -v curl
  [ "$?" -eq 0 ]
  # json parser
  command -v jq
  [ "$?" -eq 0 ]
  # git binary
  command -v git
  [ "$?" -eq 0 ]
  # setup database
  docker inspect $(container_id "postgres") > /dev/null || {
    init_database;
  }
  # setup redis
  docker inspect $(container_id "redis") > /dev/null || {
    init_redis;
  }
}

# fetch "POST" "data1=one&data2=two" "url" "http://server/endpoint"
function fetch() {
  tmp=$(mktemp)

  unset params
  unset field_params

  [ -f "$CURL_PARAMS" ] && params=$(cat $CURL_PARAMS)
  if [[ "$3" == "url" ]]; then field_params="-d '$2'"
  else
    IFS='&' read -ra reqparams <<< "$2"
    for reqparam in ${reqparams[@]}; do
      field_params="$field_params -F '$reqparam'"
    done
  fi
  params="$params $field_params -k -D $tmp -s -X $1"

  echo "curl $params $4"
  export HTTP_BODY=$(eval "curl $params $4")
  export HTTP_STATUS_CODE=$(head -n1 $tmp |cut -d' ' -f2)
  echo "HTTP_STATUS_CODE = $HTTP_STATUS_CODE"
  echo "HTTP_BODY = $HTTP_BODY"

  rm -v $tmp
  [ "$?" -eq 0 ]
}

# post_form "data1=one&data2=two" "http://server/endpoint"
function post_form() {
  fetch "POST" "$1" "form" "$2"
}

# post "data1=one&data2=two" "http://server/endpoint"
function post() {
  fetch "POST" "$1" "url" "$2"
}

# get "http://server/endpoint?data1=one&data2=two"
function get() {
  fetch "GET" "" "url" "$1"
}

# start_app "g1" "3000" "diaspora:v1.0.1"
function start_app() {
  params=""
  if [ ! -z ${PRSHA} ]; then
    echo "#  +++ Custom build active +++" >&3
    echo "# Project $PROJECT ($PRREPO)" >&3
    echo "# With SHA $PRSHA" >&3
    params="-e PROJECT=$PROJECT -e PRSHA=$PRSHA -e PRREPO=$PRREPO"
  fi
  cid=$(container_id "$1")
  echo "Starting docker container $cid (${CI_REGISTRY_IMAGE}/$3) on port $2"
  docker run --name=$cid $params \
    --link $(container_id "postgres"):postgreshost \
    --link $(container_id "redis"):redishost \
    -e DATABASE=$1 -e PORT=$2 -d ${CI_REGISTRY_IMAGE}/$3
}

# stop_app "g1"
function stop_app() {
  docker kill "$(container_id "$1")"
}

# remove_app "g1"
function remove_app() {
  # print the entire container log before
  # deleting for debugging purposes
  echo -e "#\n#\n# CONTAINER LOG $(container_id "$1")\n#\n#" >&3
  docker logs "$(container_id "$1")" 2>&1 \
    | while read line; do echo "# $line" >&3; done
  # delete the container
  docker rm "$(container_id "$1")"
}

# container_id "g1"
function container_id() {
  # set a gloabl test ID for multiple project testing
  if [ ! -f "$TEST_ID" ]; then
    shuf -i 10000-90000 -n 1 > $TEST_ID
  fi
  echo "$(cat $TEST_ID)-$1"
}

# container_ip "g1"
function container_ip() {
  docker inspect \
    -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
    "$(container_id "$1")"
}

# wait_for_mastodon "m1"
function wait_for_mastodon() {
  cid=$(container_id "$1")
  function cmd() {
    docker logs $cid 2>&1 |grep 'webpack: Compiled successfully' |wc -l
  }
  wait_for cmd "2"
}

# wait_for "docker logs g1" "Listening on" 120
function wait_for() {
  timeout=500
  if [[ "$3" =~ ^[0-9]+$ ]] ; then
    timeout=$3
  fi
  command -v $1 >/dev/null
  if [ "$?" -ne 0 ]; then echo 1; return; fi

  i=0; while true; do
    if [ "$i" -gt "$timeout" ]; then break; fi
    ((i++))
    sleep 1
  done & >/dev/null 2>&1
  TIMEOUT_PID=$!

  while true; do
    res=$(eval $1 2>&1); code=$?
    echo "$res" |grep -m 1 "$2" >/dev/null
    [ "$?" -eq 0 ] && [ "$code" -eq 0 ] && break
    sleep 1
  done & >/dev/null 2>&1
  CMD_PID=$!

  while true; do
    # check timeout child
    kill -0 $TIMEOUT_PID >/dev/null 2>&1
    if [ "$?" -gt 0 ]; then
      kill -9 $CMD_PID
      echo 1
      break
    fi
    # check cmd child
    kill -0 $CMD_PID >/dev/null 2>&1
    if [ "$?" -gt 0 ]; then
      kill -9 $TIMEOUT_PID
      echo 0
      break
    fi
    sleep 1
  done
}

# query "g1" "select count(*) from posts;"
function query() {
  out=$(psql -t -h $(container_ip "postgres") -d $1 -U postgres -c "$2")
  code=$?
  echo -ne $out
  return $code
}

# init_database will be triggered on setup()
function init_database() {
  docker run -d --name $(container_id postgres) postgres:latest
  cmd="docker logs $(container_id postgres)"
  text="ready for start up"
  code=$(wait_for "$cmd" "$text")
  [ "$code" -eq "0" ]
}

# init_redis will be triggered on setup()
function init_redis() {
  docker run -d --name $(container_id redis) redis:latest
  cmd="docker logs $(container_id redis)"
  text="Ready to accept connections"
  code=$(wait_for "$cmd" "$text")
  [ "$code" -eq "0" ]
}

# create_database "g1"
function create_database() {
  query "postgres" "create database $1;"
}

# drop_database "g1"
function drop_database() {
  query "postgres" "drop database $1;"
}

# json_value "ID"
function json_value() {
  echo $HTTP_BODY | jq -r ".$1"
}

# latest_tag "diaspora"
function latest_tag() {
  git tag |sort -r |while read tag; do
    label=$(echo $tag |cut -d- -f2)
    if [ "$label" == "$1" ]; then
      echo :$tag
      break
    fi
  done
}

# rails_runner "d1" "puts 'Hello World!'"
function rails_runner() {
  docker exec "$(container_id "$1")" bundle exec rails runner "$2"
}
